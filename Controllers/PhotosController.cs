using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Photorizer.Data.Services;
using Photorizer.Models;

namespace Photorizer.Controllers
{
    [Route("api/[controller]")]

    public class PhotosController : Controller
    {
        private readonly PhotosService _photosService;
        private readonly ImagesService _imagesService;

        public PhotosController(PhotosService photosService, ImagesService imagesService)
        {
            _photosService = photosService;
            _imagesService = imagesService;
        }

        [HttpGet]
        public IEnumerable<Photo> Index() => _photosService.GetAll();

        [HttpGet("{id}")]
        public Photo Get(int id) => _photosService.Get(id);

        [HttpPost]
        public Photo Post([FromBody]Photo photo) => _photosService.Add(photo);

        [HttpPut("{id}")]
        public Photo Put([FromBody]Photo photo) => _photosService.Update(photo);

        [HttpDelete("{id}")]
        public void Delete([FromBody]Photo photo) => _photosService.Delete(photo);

        [HttpDelete("[action]")]
        public void DeleteAll() => _photosService.DeleteAll();
        
        [HttpPost("[action]")]
        public async Task<IActionResult> Upload(IFormFile file)
        {
            if (file == null || file.Length == 0) return NoContent();

            var photo = await _imagesService.SaveAsync(file);
            var savedPhoto = _photosService.Add(photo);

            return Ok(savedPhoto);
        }

        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> DetectLabels(int id)
        {
            var photo = _photosService.Get(id);
            if (photo == null) return NotFound();
            
            var labels = await _imagesService.DetectLabelsAsync(photo);

            return Ok(labels);
        }
    }
}
