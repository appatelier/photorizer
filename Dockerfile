# FROM microsoft/dotnet
FROM gcr.io/google-appengine/aspnetcore:1.1

RUN apt-get update
RUN wget -qO- https://deb.nodesource.com/setup_6.x | bash -
RUN apt-get install -y build-essential nodejs

COPY . /app
# ADD ./ /app                                                                                                                                                                                           

WORKDIR /app

# ENV ASPNETCORE_ENVIRONMENT docker
ENV ASPNETCORE_URLS=http://*:${PORT}
ENV ASPNETCORE_URLS http://+:8080
ENV GOOGLE_APPLICATION_CREDENTIALS=google-credentials.json                                                                                                                                                              

# RUN ["dotnet", "restore"]
# RUN ["dotnet", "build"]

# EXPOSE 5000/tcp

# CMD ["dotnet", "run", "--server.urls", "http://*:5000"]
ENTRYPOINT [ "dotnet", "Photorizer.dll" ]
