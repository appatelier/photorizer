using System.Collections.Generic;

namespace Photorizer.Data.Services
{
    public interface IService<T>
    {
        IEnumerable<T> GetAll();
        T Get(int id);
        T Add(T newEntity);
        T Update(T entity);
        void Delete(T entity);
        void DeleteAll();
    }
}
