using Google.Cloud.Storage.V1;
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Photorizer.Data.Services
{
    public interface IImageUploader
    {
        Task<String> UploadImageAsync(IFormFile image, string id);
        Task DeleteUploadedImage(string id);
    }
}
