using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Photorizer.Models;

namespace Photorizer.Data.Services
{
    public class PhotosService : IService<Photo>
    {
        private ApplicationDbContext _dbContext;

        public PhotosService(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IEnumerable<Photo> GetAll() => _dbContext.Photos.AsEnumerable();

        public Photo Get(int id) => _dbContext.Photos.Find(id);

        public Photo Add(Photo entity)
        {
            var createdEntity = _dbContext.Add(entity);
            _dbContext.SaveChanges();

            return createdEntity.Entity;
        }

        public Photo Update(Photo entity)
        {
            _dbContext.Entry(entity).State = EntityState.Modified;
            _dbContext.SaveChanges();

            return entity;
        }

        public void Delete(Photo entity) 
        {
            _dbContext.Photos.Remove(entity);
            _dbContext.SaveChanges();
        }

        public void DeleteAll() 
        {
            var entities = _dbContext.Photos;
            _dbContext.Photos.RemoveRange(entities);
            _dbContext.SaveChanges();
        }
    }
}
