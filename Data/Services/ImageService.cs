using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Google.Cloud.Vision.V1;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Photorizer.Models;

namespace Photorizer.Data.Services
{
    public class ImageService
    {
        private readonly string _photosDirectory;
        private readonly IImageUploader _imageUploader;

        public ImageService(IImageUploader imageUploader)
        {
            _imageUploader = imageUploader;
        }

        public async Task<Photo> SaveAsync(IFormFile file) {
            var photo = Photo.Create(file);

            photo.AbsolutePath = await _imageUploader.UploadImageAsync(file, photo.UniqueName);
            photo.ImageSaved = true;

            return photo;
        }

        public async Task<IEnumerable<string>> DetectLabelsAsync(Photo photo)
        {
            var image = Image.FromUri(photo.AbsolutePath);
            
            var client = ImageAnnotatorClient.Create();
            var labelAnnotations = await client.DetectLabelsAsync(image);

            return labelAnnotations
                    .Where(a => a.Description != null)
                    .Select(a => a.Description);

        } 
    }
}
