using Google.Cloud.Storage.V1;
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Photorizer.Data.Services
{
    public class GoogleGloudImageUploader : IImageUploader
    {
        private readonly string _bucketName;
        private readonly StorageClient _storageClient;

        public GoogleGloudImageUploader()
        {
            _bucketName = "invertible-fin-174814";
            _storageClient = StorageClient.Create();
        }

        public async Task<String> UploadImageAsync(IFormFile image, string id)
        {
            var imageAcl = PredefinedObjectAcl.PublicRead;

            var imageObject = await _storageClient.UploadObjectAsync(
                bucket: _bucketName,
                objectName: id,
                contentType: image.ContentType,
                source: image.OpenReadStream(),
                options: new UploadObjectOptions { PredefinedAcl = imageAcl }
            );

            return imageObject.MediaLink;
        }

        public async Task DeleteUploadedImage(string id)
        {
            try
            {
                await _storageClient.DeleteObjectAsync(_bucketName, id);
            }
            catch (Google.GoogleApiException exception)
            {
                if (exception.Error.Code != 404) throw;
            }
        }
    }
}
