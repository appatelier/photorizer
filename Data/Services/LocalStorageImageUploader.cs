using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Photorizer.Models;
using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace Photorizer.Data.Services
{
    public class LocalStorageImageUploader : IImageUploader
    {
        private readonly string _photosDirectory;
        private readonly IImageUploader _imageUploader;
        private readonly IHostingEnvironment _environment;

        public LocalStorageImageUploader(IHostingEnvironment environment, IImageUploader imageUploader)
        {
            _photosDirectory = "photos";
            _imageUploader = imageUploader;
            _environment = environment;
        }

        public async Task<String> UploadImageAsync(IFormFile image, string id)
        {
            var photo = Photo.Create(image);
            var path = AbsoluteImagePath(photo);

            using (var fileStream = new FileStream(path, FileMode.Create))
            {
                await image.CopyToAsync(fileStream);
            }

            return path;
        }

        public Task DeleteUploadedImage(string id) => throw new NotImplementedException();

        private string AbsoluteImagePath(Photo photo) => Path.Combine(_environment.WebRootPath, _photosDirectory, photo.UniqueName);     
        private string RelativeImagePath(Photo photo) => Path.Combine(_photosDirectory, photo.UniqueName);
    }
}
