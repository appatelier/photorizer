﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Photorizer.Data;

namespace Photorizer.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20170727154222_PhotoEntityEnhancement")]
    partial class PhotoEntityEnhancement
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn)
                .HasAnnotation("ProductVersion", "1.1.2");

            modelBuilder.Entity("Photorizer.Models.Photo", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("AbsolutePath");

                    b.Property<bool>("ImageSaved");

                    b.Property<int>("Length");

                    b.Property<string>("Name");

                    b.Property<string>("RelativePath");

                    b.Property<string>("UniqueName");

                    b.HasKey("Id");

                    b.ToTable("Photos");
                });
        }
    }
}
