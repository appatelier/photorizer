import 'rxjs/add/operator/map';

import { Inject, Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { IPhoto } from '../domain/photo';

@Injectable()
export class PhotosService {
  private readonly rootUrl: string;

  constructor(private http: Http, @Inject('ORIGIN_URL') originUrl: string) {
    this.rootUrl = `${originUrl}/api/Photos`;
  }

  query(): Observable<IPhoto[]> {
    return this.http.get(this.rootUrl)
      .map(res => res.json() as IPhoto[])
  }

  getPhoto(id: number): Observable<IPhoto> {
    return this.http.get(`${this.rootUrl}/${id}`)
      .map(res => res.json() as IPhoto)
  }

  delete(photo: IPhoto): Observable<Response> {
    return this.http.delete(`${this.rootUrl}/${photo.id}`, { body: photo });
  }

  detectLabels(id: number): Observable<string[]> {
    return this.http.get(`${this.rootUrl}/DetectLabels/${id}`)
      .map(res => res.json() as string[])
  }
}
