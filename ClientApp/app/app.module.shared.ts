import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FileDropDirective, FileSelectDirective } from 'ng2-file-upload';

import { AppComponent } from './components/app/app.component';
import { HomeComponent } from './components/home/home.component';
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { PhotoComponent } from './components/photo/photo.component';
import { PhotosComponent } from './components/photos/photos.component';
import { UploaderComponent } from './components/uploader/uploader.component';

export const sharedConfig: NgModule = {
  bootstrap: [AppComponent],
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    UploaderComponent,
    PhotosComponent,
    PhotoComponent,
    FileSelectDirective,
    FileDropDirective
  ],
  imports: [
    RouterModule.forRoot([
      { path: '', redirectTo: 'photos', pathMatch: 'full' },
      { path: 'uploader', component: UploaderComponent },
      { path: 'photos', component: PhotosComponent },
      { path: 'photo/:id', component: PhotoComponent },
      { path: '**', redirectTo: 'photos' }
    ])
  ]
};
