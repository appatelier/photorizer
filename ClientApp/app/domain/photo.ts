export interface IPhoto {
  id: number;
  name: string;
  uniqueName: string;
  relativePath?: string;
  absolutePath?: string;
  length?: number;
  title?: string;
  description?: string;
  shortDescription?: string;
  style?: object | string;
}
