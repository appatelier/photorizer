import { Component } from '@angular/core';
import { Observable } from 'rxjs/Rx';

import { IPhoto } from '../../domain/photo';
import { PhotosService } from '../../services/photos.service';

@Component({
  selector: 'photos',
  templateUrl: './photos.component.html.hamlc',
  styleUrls: ['./photos.component.scss'],
  providers: [PhotosService]
})
export class PhotosComponent {
  photos: IPhoto[];

  constructor(private photosService: PhotosService) {
    this.photosService.query().subscribe(photos => {
      photos.forEach(photo => photo.style = {'background-image': `url(${photo.absolutePath})`});
      this.photos = photos.filter(photo => photo.absolutePath);
      console.log(photos);
    });
  }
}
