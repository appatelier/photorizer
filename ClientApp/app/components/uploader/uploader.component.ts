import { Component, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { FileUploader } from 'ng2-file-upload';

import { IPhoto } from '../../domain/photo';
import { PhotosService } from '../../services/photos.service';

@Component({
  selector: 'uploader',
  templateUrl: './uploader.component.html.hamlc',
  styleUrls: ['./uploader.component.scss'],
  providers: [PhotosService]
})
export class UploaderComponent {
  private readonly allowedTypes: string[];
  public uploader: FileUploader;
  public uploaderDropZoneHover: boolean;
  public uploadedPhoto: IPhoto;

  constructor( @Inject('ORIGIN_URL') originUrl: string, private photosService: PhotosService, private router: Router) {
    this.allowedTypes = ['image/png', 'image/jpeg'];
    this.uploaderDropZoneHover = false;
    this.uploader = new FileUploader({
      autoUpload: true,
      allowedMimeType: this.allowedTypes,
      url: `${originUrl}/api/Photos/Upload`
    });

    this.uploader.onSuccessItem = (item, response, status, headers) => {
      if (status === 200) {
        this.uploadedPhoto = JSON.parse(response) as IPhoto;
        this.router.navigate([`/photo/${this.uploadedPhoto.id}`]);
      }
    }
  }

  public clearPhoto() {
    this.uploadedPhoto = null;
  }

  public fileOverBase(e: any): void {
    this.uploaderDropZoneHover = e;
  }
}
