import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs/Rx';

import { IPhoto } from '../../domain/photo';
import { PhotosService } from '../../services/photos.service';

@Component({
  selector: 'photo',
  templateUrl: './photo.component.html.hamlc',
  styleUrls: ['./photo.component.scss'],
  providers: [PhotosService]
})
export class PhotoComponent implements OnInit, OnDestroy {
  private paramsSub: Subscription;
  photo: IPhoto;
  photoLabels: Observable<string[]>;

  constructor(private route: ActivatedRoute, private photosService: PhotosService, private router: Router) {}

  public ngOnInit(): void {
    this.paramsSub = this.route.params.subscribe(params => {
      let id =  Number(params['id']);
      this.photosService.getPhoto(id).subscribe(photo => this.photo = photo);
      this.photoLabels = this.photosService.detectLabels(id);
    });
  }

  public ngOnDestroy(): void {
    this.paramsSub.unsubscribe();
  }

  delete() {
    this.photosService.delete(this.photo).subscribe(response => {
      if (response.ok) {
        this.router.navigate(['/photos']);
      }
    });
  }

  goToList() {
    this.router.navigate(['/photos']);
  }
}
